const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv').config();

//App de Express
const app = express();
const { sequelize } = require('./database/models/index');
app.use(cors());
app.use(bodyParser.json());
//Node Server
const server = require('http').createServer(app);
app.use('/api/ejemplo', require('./controller/index.controller'));
//path público
const publicPath = path.resolve(__dirname, 'public');
app.use(express.static(publicPath));

server.listen(process.env.PORT, (err) => {
    if (err) throw new Error(err);

    sequelize.authenticate().then(() => {
        console.log("Conexion a Mysql");
    }).catch(error => {
        console.log("Error en la conexion de la base de datos", error);
    });
});