'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.addColumn(
        'Consulta',
        'CatEspecialidadId',
        {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        { transaction }
      );
      queryInterface.sequelize.query("ALTER TABLE Consulta ADD CONSTRAINT consulta_catespecialidad_id_fkey FOREIGN KEY (CatEspecialidadId) REFERENCES Cat_especialidads (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE;");
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
  down: async (queryInterface, Sequelize) => {
    //ALTER TABLE tbl_name DROP FOREIGN KEY fk_symbol;
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.removeColumn('Consulta', 'CatEspecialidadId', { transaction });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  }
};
