'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.removeColumn(
        'Receta', 
        'contenido', 
        { transaction }
      );
      await queryInterface.addColumn(
        'Receta',
        'diagnostico',
        {
          type: Sequelize.STRING(65535)
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'Receta',
        'tratamiento',
        {
          type: Sequelize.STRING(65535)
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'Receta',
        'editable',
        {
          type: Sequelize.INTEGER
        },
        { transaction }
      );
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
  down: async (queryInterface, Sequelize) => {
    //ALTER TABLE tbl_name DROP FOREIGN KEY fk_symbol;
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.addColumn(
        'Receta', 
        'contenido',
        {
          type: Sequelize.STRING(65535)
        },
        { transaction }
      );
      await queryInterface.removeColumn(
        'Receta',
        'diagnostico',
        { transaction }
      );
      await queryInterface.removeColumn(
        'Receta',
        'tratamiento',
        { transaction }
      );
      await queryInterface.removeColumn(
        'Receta',
        'editable',
        { transaction }
      );
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  }
};
