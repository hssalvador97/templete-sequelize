'use strict';
module.exports = {
    up: async(queryInterface, Sequelize) => {
        await queryInterface.createTable('Noticias', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            titulo: {
                type: Sequelize.STRING
            },
            contenido: {
                type: Sequelize.STRING(65535)
            },
            imagen: {
                type: Sequelize.STRING
            },
            fecha: {
                type: Sequelize.DATE
            },
            activo: {
                type: Sequelize.BOOLEAN
            }
        });
    },
    down: async(queryInterface, Sequelize) => {
        await queryInterface.dropTable('Noticias');
    }
};