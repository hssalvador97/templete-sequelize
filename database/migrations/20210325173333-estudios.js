'use strict';

module.exports = {
    up: async(queryInterface, Sequelize) => {

        const transaction = await queryInterface.sequelize.transaction();
        try {

            await queryInterface.removeColumn('Estudios', 'MedicoId', { transaction });
            await transaction.commit();
        } catch (err) {
            await transaction.rollback();
            throw err;
        }
    },

    down: async(queryInterface, Sequelize) => {
        const transaction = await queryInterface.sequelize.transaction();
        try {
            await queryInterface.addColumn(
                'Estudios',
                'MedicoId', {
                    type: Sequelize.INTEGER,
                    allowNull: false,
                }, { transaction }
            );
            queryInterface.sequelize.query("ALTER TABLE Estudio ADD CONSTRAINT estudio_medico_id_fkey FOREIGN KEY (MedicoId) REFERENCES Medicos (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE;");
            await transaction.commit();
        } catch (err) {
            await transaction.rollback();
            throw err;
        }
    }

};