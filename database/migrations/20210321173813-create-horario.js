'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Horarios', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      CatDiasId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Cat_dias',
          key: 'id'
        }
      },
      hora_inicio: {
        type: Sequelize.DATE
      },
      hora_fin: {
        type: Sequelize.DATE
      },
      MedicoId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Medicos',
          key: 'id'
        }
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Horarios');
  }
};