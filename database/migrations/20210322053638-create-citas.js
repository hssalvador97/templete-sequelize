'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Citas', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fecha: {
        type: Sequelize.DATE
      },
      PacienteId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Pacientes',
          key: 'id'
        }
      },
      MedicoId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Medicos',
          key: 'id'
        }
      },      
      CatEspecialidadId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Cat_especialidads',
          key: 'id'
        }
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Citas');
  }
};