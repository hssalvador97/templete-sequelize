'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Usuario extends Model {

        static associate(models) {
            // Usuario.belongsTo(models.Cat_rol);
            // Usuario.belongsTo(models.Perfil);
        }
    };
    Usuario.init({
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        usuario: DataTypes.STRING,
        correo: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        fecha_registro: DataTypes.DATEONLY,
        id_stripe: DataTypes.STRING,
        activo: DataTypes.INTEGER
    }, {
        sequelize,
        modelName: 'Usuario',
    });
    return Usuario;
};